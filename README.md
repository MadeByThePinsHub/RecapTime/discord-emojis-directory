# Discord Emojis Directory

This repository contains JSON files and PNG/GIF source files of different custom emojis
from Discord for use in Guilded, among other things.

## Contribute

Please see the `CONTRIBUTING.md` file for details.

## License

This repo is licensed under CC BY-SA 4.0, except the images under the `public/images` directory, which may
licensed under copyright by their respective copyright holders. For takedowns, please
send us an issue to remove it
